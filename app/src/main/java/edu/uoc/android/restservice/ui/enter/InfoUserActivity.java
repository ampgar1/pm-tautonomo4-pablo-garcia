package edu.uoc.android.restservice.ui.enter;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import edu.uoc.android.restservice.rest.adapter.GitHubAdapter;
import edu.uoc.android.restservice.rest.model.Followers;
import edu.uoc.android.restservice.rest.model.Owner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InfoUserActivity extends AppCompatActivity {

    ArrayList<Owner> listaFollowers;
    RecyclerView recyclerViewFollowers;

    TextView textViewRepositories, textViewFollowing;
    ImageView imageViewProfile;

    ProgressDialog progressDialog, progressDialog2;
    //Cuadros de dialogos del progress
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(edu.uoc.android.restservice.R.layout.activity_info_user);

        textViewFollowing = findViewById(edu.uoc.android.restservice.R.id.textViewFollowing);
        textViewRepositories = findViewById(edu.uoc.android.restservice.R.id.textViewRepositories);
        imageViewProfile = (ImageView) findViewById(edu.uoc.android.restservice.R.id.imageViewProfile);


        listaFollowers = new ArrayList<>();
        recyclerViewFollowers = (RecyclerView)findViewById(edu.uoc.android.restservice.R.id.recyclerViewFollowers);

        recyclerViewFollowers.setLayoutManager(new LinearLayoutManager(this));

        String loginName = getIntent().getStringExtra("loginName");
        mostrarDatosBasicos(loginName);
    }

    TextView labelFollowing, labelRepositories, labelFollowers;
    private void initProgressBar()
    {
        //progressBar.setVisibility(View.VISIBLE);
        textViewFollowing.setVisibility(View.INVISIBLE);
        textViewRepositories.setVisibility(View.INVISIBLE);
        imageViewProfile.setVisibility(View.INVISIBLE);
        recyclerViewFollowers.setVisibility(View.INVISIBLE);

        labelFollowing = (TextView)findViewById(edu.uoc.android.restservice.R.id.labelFollowing);
        labelFollowing.setVisibility(View.INVISIBLE);

        labelRepositories = (TextView) findViewById(edu.uoc.android.restservice.R.id.labelRepositories);
        labelRepositories.setVisibility(View.INVISIBLE);

        labelFollowers = (TextView) findViewById(edu.uoc.android.restservice.R.id.labelFollowers);
        labelFollowers.setVisibility(View.INVISIBLE);

    }

    private void mostrarDatosBasicos(String loginName){
        progressDialog = new ProgressDialog(this);
        //New instancia del progressdialog
        progressDialog.setMessage("Buscando usuario");
        //Mensaje el cual se mostrara cuando este Buscando usuario
        progressDialog.show();
        //Mustra el cuadro de dialog
        GitHubAdapter adapter = new GitHubAdapter();
        Call<Owner> call = adapter.getOwner(loginName);
        call.enqueue(new Callback<Owner>() {

            @Override
            public void onResponse(Call<Owner> call, Response<Owner> response) {
                progressDialog.dismiss();
               //Termina el progress dailog
                Owner owner = response.body();
                if (owner != null) {
                    // Resultados "si"
                    textViewRepositories.setText(owner.getPublicRepos().toString());
                    // Muestra el numero de repositorios
                    textViewFollowing.setText(owner.getFollowing().toString());
                    // Muestra el numero de seguidores
                    Picasso.get().load(owner.getAvatarUrl()).into(imageViewProfile);
                    // Se envia la imagen del usuario a un imageview
                }else {
                    // Resultados "si no hay resultados muestra un error"
                    Toast.makeText(InfoUserActivity.this, "Ha ocurrido un error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Owner> call, Throwable t) {
                // Cuando sucede el error , muestra el mensaje
                Toast.makeText(InfoUserActivity.this, "Ha ocurrido un error al realizar la petición", Toast.LENGTH_SHORT).show();
            }
        });

        //Se trabaja el nuevo progressdialog
        progressDialog2 = new ProgressDialog(this);
        //New instancia del progressdialog
        progressDialog2.setMessage("Buscando seguidores");
        //Mensaje el cual se mostrara cuando este Buscando usuario
        progressDialog2.show();
        //Mustra el cuadro de dialog

        // // se relaiza una llamada a los seguidores enviando un nombre a un parametro
        Call<List<Followers>> callFollowers = new GitHubAdapter().getOwnerFollowers(loginName);

        callFollowers.enqueue(new Callback<List<Followers>>() {
            @Override
            public void onResponse(Call<List<Followers>> call, Response<List<Followers>> response) {
                progressDialog2.cancel();
                List<Followers> list = response.body();
                // Se almacena en una lista con los resultados
                if (list != null) {
                    // Resultados "si", entonces envia los resultados a un adaptador para posteriomente mostrarlos
                    AdaptadorFollowers adapter = new AdaptadorFollowers(list);
                    recyclerViewFollowers.setAdapter(adapter);
                } else {
                    // Resultados "si no hay datos muestra un error"
                    Toast.makeText(InfoUserActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Followers>> call, Throwable t) {
                // Despues de no conseguir resultado se muestra este mensaje "Ha ocurrido un error al realizar la petición"
                Toast.makeText(InfoUserActivity.this, "Ha ocurrido un error al realizar la petición", Toast.LENGTH_SHORT).show();
            }
        });

    }

}
