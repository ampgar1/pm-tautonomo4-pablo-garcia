package edu.uoc.android.restservice.ui.enter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import edu.uoc.android.restservice.rest.model.Followers;

/**
 * Created by edgardopanchana on 4/29/18.
 */

public class AdaptadorFollowers extends RecyclerView.Adapter<AdaptadorFollowers.ViewHolderFollowers> {

    List<Followers> listaFollowers;  // Lista de tipo Followers
    Context context;

    public AdaptadorFollowers(List<Followers> listaFollowers) {
        // Aqui se asignan los valores de contructor a una lista
        this.listaFollowers = listaFollowers;
    }

    @NonNull
    @Override
    public ViewHolderFollowers onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(edu.uoc.android.restservice.R.layout.item_list, null, false);
        context = parent.getContext();
        return new ViewHolderFollowers(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderFollowers holder, int position) {
        holder.etiNombre.setText(listaFollowers.get(position).getLogin());
        // Eenvia los nombres a un textview
        Picasso.get().load(listaFollowers.get(position).getAvatarUrl()).into(holder.imagen);
        //envia una image a un imageview
    }

    @Override
    public int getItemCount() {
        return listaFollowers.size();
    }

    public class ViewHolderFollowers extends RecyclerView.ViewHolder {

        TextView etiNombre;// con esta variable obtenemos los textos nombres
        ImageView imagen; // con esta variable obtenemos las imagenes

        public ViewHolderFollowers(View itemView) {
            super(itemView);

            etiNombre = (TextView) itemView.findViewById(edu.uoc.android.restservice.R.id.textViewLista);
            //aqui se guardan en un text view
            imagen = (ImageView) itemView.findViewById(edu.uoc.android.restservice.R.id.imageViewLista);
            // aqui se guardan en un image view

        }
    }
}
